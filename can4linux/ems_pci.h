/************************************************************************/
/* hardware access functions or macros */
/************************************************************************/

/* using memory acces with readb(), writeb() */
/* Both CPC_PCI boards have the CAN Controllers memory mapped */

/* instead of the sruct member names, register offset values are needed */

#define	canmode 	0		/* 0 */
#define	cancmd		1
#define	canstat		2
#define	canirq		3
#define	canirq_enable	4
#define	reserved1	5		/* 5 */
#define	cantim0		6
#define	cantim1		7
#define	canoutc		8
#define	cantest		9
#define	reserved2	10		/* 10 */
#define	arbitrationlost	11	/* read only */
#define	errorcode	12		/* read only */
#define	errorwarninglimit	13
#define	rxerror		14
#define	txerror		15		/* 0x0f */
#define	frameinfo	16

#define	canrxbufferadr	30		/* 30 */;
#define	canclk		31 	 


/* frame structure */
#define frame_stdframe_canid1		17
#define frame_stdframe_canid2		18
#define frame_stdframe_candata		19

#define frame_extframe_canid1		17
#define frame_extframe_canid2		18
#define frame_extframe_canid3		19
#define frame_extframe_canid4		20
#define frame_extframe_canxdata		21



extern void CAN_OUT(int bd, int adr, unsigned char v);
extern u8 CAN_IN(int bd, int adr);
extern void  CAN_SET(int minor, int adr, int mask);
extern void  CAN_RESET(int minor, int adr, int mask);
